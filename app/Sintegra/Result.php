<?php

namespace App\Sintegra;

class Result
{
	private $titulos;
	private $valores;

	public function __construct(string $resultContent)
	{
		$this->titulos = $this->getByRegex($resultContent, "/<td.*\btitulo\b.*>(.*)<\//");
		$this->valores = $this->getByRegex($resultContent, "/<td.*\bvalor\b.*>(.*)<\//");
	}

	private function getByRegex(string $data, string $regex) :array
    {
        $returnList = [];

        foreach (explode("\n", $data) as $line) {
            if (preg_match($regex, $line, $output_array)) {
                $returnList[] = $this->sanitize($output_array[1]);
            }
        }

        return $returnList;
    }

    public function getArray() :array
    {
        $fields = [];

        foreach ($this->titulos as $key => $value) {
            $fields[$value] = $this->valores[$key];
        }

        return $fields;
    }

    private function sanitize(string $value) :string
    {
    	$value = trim($value);
    	$value = str_replace('&nbsp;', '', $value);
    	$value = str_replace(':', '', $value);

    	return $value;
    }
}
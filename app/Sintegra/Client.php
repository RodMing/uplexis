<?php

namespace App\Sintegra;

use GuzzleHttp\Client as GuzzleClient;

class Client
{
    private $sintegraUri;
    private $guzzleClient;

    public function __construct(GuzzleClient $guzzleClient)
    {
        $this->guzzleClient = $guzzleClient;
        $this->sintegraUri = env('SINTEGRA_URI');
    }

    public function findByCnpj(string $cnpj) :Result
    {
        $result = $this->guzzleClient->post(
            $this->sintegraUri,
            [
                'form_params' => [
                    'botao' => 'Consultar',
                    'num_cnpj' => $cnpj
                ]
            ]
        );

        return new Result(
            utf8_encode(
                $result->getBody()->getContents()
            )
        );
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use App\Usuario;

class AuthSintegra
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $type = 'web')
    {
        if (!session()->has('usuario') && !session()->has('senha')) {
            if (!$request->has('usuario') || !$request->has('senha')) {
                return $this->notAuthorized($type);
            }

            if (!$this->auth($request->usuario, $request->senha)) {
                return $this->notAuthorized($type);
            }
        }

        return $next($request);
    }

    private function notAuthorized(string $typeApp)
    {
        switch ($typeApp) {
            case 'web':
                return redirect()->back()->withErrors('Not Authorized');
                break;

            default:
                return response('Not Authorized', 401);
                break;
        }
    }

    private function auth($usuario, $senha) :bool
    {
        if (!is_null(Usuario::where('usuario', $usuario)->where('senha', $senha)->first())) {
            session([
                'usuario' => $usuario,
                'senha'   => $senha
            ]);

            return true;
        }

        session()->forget('usuario');
        session()->forget('senha');

        return false;
    }
}

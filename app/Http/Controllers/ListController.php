<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sintegra;

class ListController extends Controller
{
    public function index()
    {
    	return view('list', [
    		'list' => Sintegra::all()
    	]);
    }

    public function delete(Request $request)
    {
    	$sintegraEntity = Sintegra::find(
    		$request->dataId
    	);

    	if ($sintegraEntity->delete()) {
    		$mensagem = 'Sucesso';
    		$status = 200;
    	} else {
    		$mensagem = 'Erro';
    		$status = 500;
    	}

    	return response($mensagem, $status);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Sintegra;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.sintegra')->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return view('home', $request->all());
    }

    public function save(Request $request)
    {
        $usuario = session()->get('usuario');
        $senha = session()->get('senha');
        $usuarioEntity = Usuario::where('usuario', $usuario)->where('senha', $senha)->first();

        $sintegraEntity = new Sintegra;
        $sintegraEntity->id_usuario = $usuarioEntity->id;
        $sintegraEntity->cnpj = $request->cnpj;
        $sintegraEntity->json = json_encode($request->data);

        if ($sintegraEntity->save()) {
            $mensagem = 'Sucesso';
            $status = 200;
        } else {
            $mensagem = 'Erro';
            $status = 500;
        }

        return response($mensagem, $status);
    }
}

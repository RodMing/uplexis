<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sintegra extends Model
{
    protected $table = 'sintegra';
    public $timestamps = false;
    protected $connection = 'mysql';
}

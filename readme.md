# Consulta de CNPJ junto ao Sintegra/ES

## Componentes:
* Laravel 5.6
* JQuery 3.3.1
* Bootstrap 4.0.0

## Requisitos:
* PHP >= 7.1.3
* npm
* php-mysql
* php-mbstring

## Vamos começar:
```
bash:$ composer install
bash:$ npm install
bash:$ chmod 777 storage
bash:$ cp .env.example .env
bash:$ ./artisan key:generate
```
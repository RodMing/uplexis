<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', 'HomeController');
Route::post('/save', 'HomeController@save')->middleware('auth.sintegra');
Route::get('/listar', 'ListController@index');
Route::post('/listar', 'ListController@delete');
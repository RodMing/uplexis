@extends('layouts.app')

@section('title', 'Login')

@section('content')
	<link href="css/login.css" rel="stylesheet" type="text/css">

    <form class="col-md-4 col-md-offset-6" method="post">
    	@if($errors->any())
            <div class="alert alert-danger" role="alert">
                <strong>ERRO</strong> {{$errors->first()}}
            </div>
        @endif
    	<div class="form-row">
		    <div class="col">
	    		<input type="text" class="form-control" id="usuario" name="usuario" placeholder="usuário">
		    </div>
	  	</div>
	  	<div class="form-row">
		    <div class="col">
    			<input type="password" class="form-control" id="senha" name="senha" placeholder="senha">
		    </div>
	  	</div>
  		<button type="submit" class="btn btn-primary">Entrar</button>
	</form>
@endsection
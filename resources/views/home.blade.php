@extends('layouts.app')

@section('title', 'Home')

@section('content')
	<link href="css/home.css" rel="stylesheet" type="text/css">
	<form class="col-md-5 col-md-offset-2" action="busca" method="post">
		<input type="hidden" id="usuario" name="usuario" value="{{ $usuario }}"/>
		<input type="hidden" id="senha" name="senha" value="{{ $senha }}"/>
		<div class="form-row">
			<div class="col-9">
				<a href="listar" class="btn btn-info">Listagem</a>
			</div>
		</div>
    	<div class="form-row">
		    <div class="col-6">
	    		<input type="text" class="form-control" id="cnpj" name="cnpj" placeholder="CNPJ">
		    </div>
		    <div class="col-3">
  				<button type="submit" class="btn btn-primary">Buscar</button>
		    </div>
	  	</div>
  		<div id="result"></div>
	</form>
	<script src="js/home.js"></script>
@endsection

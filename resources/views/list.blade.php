@extends('layouts.app')

@section('title', 'Login')

@section('content')
	<table border="1px">
	    @foreach ($list as $item)
	    	<tr style="height: 10px">
	    		<td>
	    			{{ $item->cnpj }}
	    		</td>
	    		<td>
	    			{{ $item->json }}
	    		</td>
	    		<td>
	    			<button class="btn btn-danger" data_id="{{ $item->id }}">Excluir</button>
	    		</td>
	    	</tr>
		@endforeach
	</table>
	<script src="js/list.js"></script>
@endsection
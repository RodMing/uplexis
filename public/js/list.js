$(document).ready(function() {
	$('button').click(function(event){
		event.preventDefault();

		dataId = $(this).attr('data_id');

		$.post("listar",
	    {
	        "dataId": dataId
	    },
	    function(data, status){
	    	if (status == "success") {
	    		location.reload();
	    	}
	    });
	});
});
$(document).ready(function() {
	$('button').click(function(event){
		event.preventDefault();

		usuario = $('#usuario').val();
		senha = $('#senha').val();
		cnpj = $('#cnpj').val();
		$('#result').empty();

		$.ajax({
			url: "api/sintegra/es/" + cnpj + "?usuario=" + usuario + "&senha=" + senha
		}).done(function(data, status) {
			$.each(data, function(i, item) {
				$('#result').append("<p><b>" + i + "</b>: " + item + "</p>");
			});

			if (!jQuery.isEmptyObject(data)) {
				saveData(data);
			}
		});
	});
});

function saveData(data) {


	usuario = $('#usuario').val();
	senha = $('#senha').val();
	cnpj = $('#cnpj').val();

	$.post("save",
    {
        "usuario": usuario,
        "senha": senha,
        "cnpj": cnpj,
        "data": data
    },
    function(data, status){
        console.log(data);
    });
}